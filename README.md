Migrated to [codeberg.org/diwesser/calendar-kiosk](https://codeberg.org/diwesser/calendar-kiosk)

***Warning: This repo is just a dump of a bunch of files that I haven't looked at in two years. User discretion is advised.***

# Calendar Kiosk

Scripts for setting up and running a non-interactive calendar kiosk on Linux.

This set of scripts was originally inspired by the blog post [*Ubuntu 16.04.2 Kiosk Mode*](https://michastechblog.blogspot.com/2017/04/ubuntu-16042-kiosk-mode-complete-example.html).  

# Stray Notes
- [Raspberry Pi: Wall Mounted Calendar and Notification Center](https://www.instructables.com/id/Raspberry-Pi-Wall-Mounted-Calender-and-Notificatio/) -- There are a lot of good page design examples in the photos of what other people have made.

- `gnome-calendar` driven dependancies
  - `gnome-calendar` -- Duh
  - `libglib2.0-bin` for `gsettings`, used to change theme
