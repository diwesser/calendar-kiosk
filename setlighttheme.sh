#!/bin/bash

# Sets light GTK theme for device

gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita'
gsettings set org.gnome.desktop.wm.preferences theme 'Adwaita'
