#!/bin/bash

# Sets dark GTK theme for device

gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.wm.preferences theme 'Adwaita-dark'
