#!/bin/bash
# This script provisions a Ubuntu 18.04 web kiosk using openbox and firefox
# It is designed for 18.04, but will likely work on any system with systemd and apt
# This script must be run as root

# Resources
# - Ubuntu 16.04 Kiosk Mode -- Michas Tec Blog
#   This script is pretty much the blog post in script form with my preferences.
#   https://michastechblog.blogspot.com/2017/04/ubuntu-16042-kiosk-mode-complete-example.html
# - Replace string matching regex bash
#   https://www.cyberciti.biz/faq/unix-linux-replace-string-words-in-many-files/
#   Have fun: https://regexr.com/
# - If you have problems with xauth and .Xauthority again
#   https://unix.stackexchange.com/a/215559
#   https://unix.stackexchange.com/a/298630

# Steps
#  1. Check if root
#  2. Set timezone to UTC
#  3. Add repos, update everything, install software
#  4. Create kiosk user
#  5. Create /home/kiosk/.config/diwesser/device-id
#  6. Install firefox kiosk script
#  7. Install web page
#  8. Automatically execute kiosk script when kiosk user logs in
#  9. Automatically login to firt terminal as kiosk user
# 10. Tell user to install ForceFull extension (forces full screen)
#    https://addons.mozilla.org/en-US/firefox/addon/forcefull/

# Features to add:
# - Include fullscreen settings in script
#   Maybe https://askubuntu.com/a/898461
#   Alternately, try Midori https://wiki.xfce.org/midori/faq#modes
# - Setup unattended upgrades
# - Disable display sleep
# - Toggle dark mode based on time of day

#Check if run as root
if [ "$UID" -ne "0" ] ; then # Actual check
    echo "This script must be run as root. (Use sudo.)"
    exit 1 # Stop script if 
fi

# Set timezone

# add universe repo (needed for openbox), update system, and install software
add-apt-repository universe

apt update
apt upgrade

apt install --no-install-recommends xorg openbox firefox

# Create new user and group to run the kiosk
adduser kiosk

# Create device idea file
mkdir -p /home/kiosk/.config/diwesser
touch /home/kiosk/.config/diwesser/device-id

# Write kiosk script to /home/kiosk/startkiosk.sh
#   ==TODO==

#chown /home/kiosk/.Xauthority kiosk
#chgrp /home/kiosk/.Xauthority kiosk

# Start kiosk script at kiosk user login
echo '' >> /home/kiosk/.profile
echo '# Start Firefox kiosk' >> /home/kiosk/.profile
echo '/usr/bin/startx /etc/X11/Xsession /home/kiosk/startkiosk.sh -- :0' >> /home/kiosk/.profile

# Login to kiosk on startup
cp /lib/systemd/system/getty@.service /lib/systemd/system/getty@tty1.service
# The following line isn't tested and probably doesn't work
# It should change this:
#     ExecStart=-/sbin/agetty -o '-p -- \\u' --noclear %I $TERM
# into this:
#     ExecStart=-/sbin/agetty -a kiosk --noclear %I $TERM

#sed -i "/ExecStart=*/ExecStart=-/sbin/agetty -a kiosk --noclear %I $TERM/" /lib/systemd/system/getty@tty1.service
