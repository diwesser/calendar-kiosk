#!/bin/bash

# This script is used to start a web kiosk
# It is addapted from the one found in this blog post
# https://michastechblog.blogspot.com/2017/04/ubuntu-16042-kiosk-mode-complete-example.html

# !!!UNTESTED CHANGES!!!
# - $nowEpoc in refreshTab function no longer uses non system time.
# - local $time aliases moved to global $inTimeZone

# Minor updates not pushed to device
# - Minimum uptime to refresh moved to 59 seconds

# Todo:
# - Fix refreshTab function
# - Test new xdotool flags
# - Refactor toggleDarkMode with refreshTab logic
# - Kill processes forked from function if Firefox crashes
# - Improve on the nested while true loops
#   Make the inner loop check if firefox is running, break if it isn't.
#   I think this one is now moot?


dayStart=0630
nightStart=2030
nightMode=false
isDay=true
tabRefreshTime=03:00

timezone="America/Halifax"
inTimeZone="$(env TZ="$timezone" date +"%H%M")"

###############################################################################
# Functions
###############################################################################

# Check if the dark mode should be enable and enable it if needed.
# Takes no input. Reads $nightStart, and $dayStart. Changes $nightMode, $isDay.
# Sends command to toggle dark mode.
# Retries every 15 minutes.

toggleDarkMode(){
while true ; do
    # If day
    if [[ $inTimeZone -ge $dayStart && $inTimeZone -lt $nightStart ]] ; then
        # If night mode if on in the day
        if [[ $nightMode == true ]] ; then
            xdotool key "alt+shift+d"
            #xdotool search --sync --onlyvisible --class "Firefox" \
            #    windowactivate key "alt+shift+d"
            nightMode=false
        fi
    # If night
    else
        # If night mode is off at night
        if [[ $nightMode == false ]] ; then
            xdotool key "alt+shift+d"
            #xdotool search --sync --onlyvisible --class "Firefox" \
            #    windowactivate key "alt+shift+d"
            nightMode=true
        fi
    fi
    
    # Recheck every 15 minutes
    sleep 900
done
}


# Calculate time until next refresh. Refresh at that time.
# Takes no input. Reads $tabRefreshTime.

refreshTab(){

# !!! NOT WORKING !!!
# Currently refreshes once per minute.

    # Loop calculation and refreshing
    while true ; do

        # Get current epoc time
        local nowEpoc=$(date +%s)
        # Get epoc time of next refresh
        local sleepEpoc=$($inTimeZone date -d "$tabRefreshTime" +%s)

        # Find time to next refresh
        #local sleepTime
        (( sleepTime = $sleepEpoc - $nowEpoc ))

        # If sleep time is before current time, add 24 hours.
        if [[ $sleepTime < 59 ]] ; then
            (( sleepTime = $sleepTime + 86400))
        fi

        sleep $sleepTime
        # Refresh
        xdotool key "ctrl+r"

        # Wait for a minute to avoid refreshing more than once per day
        sleep 60
    done
}


postFirefoxStart(){
    # Give Firefox lots of time to start
    sleep 180

    # Redundant with the --kiosk flag
    # Fullscreen firefox
    #xdotool search --sync --onlyvisible --class "Firefox" \
    #    windowactivate key F11
    #xdotool key F11

    # Start dark mode toggle function
    toggleDarkMode &

    # Start refresh tab function
    #refreshTab &
}

###############################################################################
# Do things
###############################################################################

# Start window manager, etc.
openbox-session &

# Keep screen on
xset -dpms     # Disable DPMS (Energy Star) features
xset s off     # Disable screensaver
xset s noblank # Don't blank video device

# Rotate display
xrandr -o left

while true ; do
    killall firefox

    # Start scrip that makes firefox run propperly.
    # Started in advance to allow full restart if Firefox crashes
    postFirefoxStart &

    #firefox "https://darksky.net/forecast/44.6486,-63.5859/us12/en"
    firefox --kiosk "https://www.icloud.com/#calendar"

done
