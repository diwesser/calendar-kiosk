#!/bin/bash

# This script is used to a calDAV calendar kiosk using Gnome Calendar.
# It is addapted from the one found in this blog post
# https://michastechblog.blogspot.com/2017/04/ubuntu-16042-kiosk-mode-complete-example.html

# Start window manager, etc.
openbox-session &

# Keep screen on
xset -dpms     # Disable DPMS (Energy Star) features
xset s off     # Disable screensaver
xset s noblank # Don't blank video device

# Rotate display
xrandr -o left

while true ; do
    killall gnome-calendar
    gnome-calendar
done
